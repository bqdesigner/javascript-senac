var segundoSemestre = [
    { materia: "Banco de dados", dia : "Segunda-feira", professor: "Professor 1", qtdAulas: '2'},
    { materia: "Javascript", dia : "Terça-feira", professor: "Professor 2", qtdAulas: '2'},
    { materia: "PHP", dia : "Quarta-feira", professor: "Professor 3", qtdAulas: '2'},
    { materia: "PI 2", dia : "Quinta-feira", professor: "Professor 4", qtdAulas: '2'},
    { materia: "Redes", dia : "Sexta-feira", professor: "Professor 5", qtdAulas: '2'}
];


// Adicionando classe container no body
var corpo = document.body;
corpo.classList.add('container');

// Criando a tabela
var table = document.createElement("table");
var thead = document.createElement("thead");
var th = document.createElement("th");
var tr = document.createElement("tr");
var tr2 = document.createElement("tr");

// Montando cabeçalho da tabela
var th1 = document.createElement("th");
var th2 = document.createElement("th");
var th3 = document.createElement("th");
var th4 = document.createElement("th");

var thtitulo1 = document.createTextNode('Matéria');
var thtitulo2 = document.createTextNode('Dia');
var thtitulo3 = document.createTextNode('Professor');
var thtitulo4 = document.createTextNode('QTD Aulas');

th1.appendChild(thtitulo1);
th2.appendChild(thtitulo2);
th3.appendChild(thtitulo3);
th4.appendChild(thtitulo4);

tr.appendChild(th1);
tr.appendChild(th2);
tr.appendChild(th3);
tr.appendChild(th4);

// Inserindo os dados linha 1
var materia1 = document.createTextNode(segundoSemestre[0].materia);
var dia1 = document.createTextNode(segundoSemestre[0].dia)
var prof1 = document.createTextNode(segundoSemestre[0].professor);
var qtdAulas1 = document.createTextNode(segundoSemestre[0].qtdAulas);

var tdInfo1 = document.createElement("td")
var tdInfo2 = document.createElement("td")
var tdInfo3 = document.createElement("td")
var tdInfo4 = document.createElement("td")

tdInfo1.appendChild(materia1);
tdInfo2.appendChild(dia1);
tdInfo3.appendChild(prof1);
tdInfo4.appendChild(qtdAulas1);

tr2.appendChild(tdInfo1);
tr2.appendChild(tdInfo2);
tr2.appendChild(tdInfo3);
tr2.appendChild(tdInfo4);

// Inserindo os dados dentro da tabela
table.appendChild(thead);
thead.appendChild(tr);
table.appendChild(tr2);


// for (var i = 0; i < 4; i++) {  
//     var tr = document.createElement("tr");
//     var td1 = document.createElement("td");
//     var td2 = document.createElement("td")

//     var text1 = document.createTextNode(i+1);
//     var text2 = document.createTextNode(segundoSemestre[0].materia);

//     td1.appendChild(text1);
//     td2.appendChild(text2);
//     tr.appendChild(td1);
//     tr.appendChild(td2);

//     table.appendChild(tr);
// }


document.body.appendChild(table);
table.classList.add('table');

