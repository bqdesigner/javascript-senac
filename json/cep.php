<?php



if (isset($_GET['cep'])) {
    $numCep = $_GET['cep']; 
    class Cep {
        public $cep;
        public $end;
        public $bairro;
    }
    if ($numCep == "06820300") {
        $end = new Cep();
        $end->cep = "06820300";
        $end->end = "Rua Minas Gerais";
        $end->bairro = "Jardim Emílio Carlos";
    } else {
        $end = new Cep();
        $end->cep = "06820301";
        $end->end = "Rua São Paulo";
        $end->bairro = "Estrada de Itapecerica";
    }
    echo json_encode($end);
}

?>