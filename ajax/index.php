<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Usando AJAX </title>
</head>
<body>
    <form action="verifica.php">
        <span> Nome de usuário </span>
        <input type="text" name="username" id="username">
        <span class="retorno"></span>
        <input type="submit" value="Enviar">
        <input id="checar" type="button" value="Checar">
    </form>

    <script>
        
        document.getElementById("checar").onclick = function() {
            var ajax = new XMLHttpRequest();
            var usuario = document.getElementById("usuario").value;
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200) {
                    if (ajax.responseText == "S") {
                        alert("Usuário existente");
                    } else {
                        alert("Pode usar");
                    }
                }
            }
            ajax.open("GET", "verifica.php?username="+usuario);
            ajax.send();
        }

    </script>
</body>
</html>