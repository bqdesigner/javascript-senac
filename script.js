
const segundoSemestre = {
    materia: ['Banco de dados', 'Javascript', 'PHP', 'Projeto', 'Redes', 'EAD', 'Conceitos de computação', 'Teste'],
    dia: ['Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feita', 'Online', 'A definir', 'Domingo'],
    professor: ['Professor 1', 'Professor 2', 'Professor 3', 'Professor 4', 'Professor 5', 'Professor 6', 'Thiago Claro'],
    qtdAulas: [2, 1, 2, 2, 2, 3, 2],
}

// var primMateria = document.getElementById("mat1");
// var primDia = document.getElementById("dia1");
// var primProf = document.getElementById("prof1");

// primMateria.innerHTML = segundoSemestre.materia[0];
// primDia.innerHTML = segundoSemestre.dia[0];
// primProf.innerHTML = segundoSemestre.professor[0];

// var segMateria = document.getElementById("mat2");
// var segDia = document.getElementById("dia2");
// var segProf = document.getElementById("prof2");

// segMateria.innerHTML = segundoSemestre.materia[1];
// segDia.innerHTML = segundoSemestre.dia[1];
// segProf.innerHTML = segundoSemestre.professor[1];

// var terMateria = document.getElementById("mat3");
// var terDia = document.getElementById("dia3");
// var terProf = document.getElementById("prof3");

// terMateria.innerHTML = segundoSemestre.materia[2];
// terDia.innerHTML = segundoSemestre.dia[2];
// terProf.innerHTML = segundoSemestre.professor[2];

// var table = document.getElementById('minhaTabela');
// var numLinhas = table.rows.length;
// var numColunas = table.rows[numLinhas - 1].cells.length;


// var novaLinha = table.insertRow(numLinhas);

// for (var j = 0; j < numColunas; j++) {
//     novaCelula = novaLinha.insertCell(j);
//     novaCelula.innerHTML = "<b>" + numLinhas + "</b>" + " – Coluna "+ j;
// }


// Montando a tabela
var corpo = document.querySelector('body');
corpo.classList.add('container');
document.write("<table>")
document.write("<tr><th>#</th><th>Matéria</th><th>Dia</th><th>Professor</th><th>Hora/Aula </th> <th>Carga horária </th> </tr>");
var tabela = document.querySelector('table');
tabela.classList.add('table');

// Interando os dados od objeto dentro da tabela
for (var i = 0; i < segundoSemestre.materia.length; i++) {
    document.write("<tr><td>" + (i+1) + "</td><td>" + segundoSemestre.materia[i] + "</td><td>" + segundoSemestre.dia[i] + "</td><td>" + segundoSemestre.professor[i] + "</td><td>" + segundoSemestre.qtdAulas[i] + "</td><td>" + segundoSemestre.qtdAulas[i] * 10 + " horas" + "</td></tr>");
}

// Finalizando a tabela
document.write("</table>")

// Alterando cor do título a cada 1s
function mudarCorBg() {
    document.body.classList.toggle('active');
}
// setInterval(mudarCorBg, 1000);
